﻿using UnityEngine;

public class RotationTrigger : MonoBehaviour
{
    public Rigidbody2D stickRB;
    public Transform target;
    public float forceValue;
    public Vector2 direction;
    public LayerMask layerMask;

    public bool drawGizmo;
    public bool useObjectForForce;
    public bool applyGradient;
    public bool whirlwind;
    //public float gradientMultiplier;

    private Vector2 dir;
    private float force;
    private void Start()
    {
        if (!stickRB) {
            stickRB = GameObject.Find("Stick").GetComponent<Rigidbody2D>();
        }
        if (useObjectForForce)
            dir = target.localPosition;
        else
            dir = direction;
        force = forceValue;
    }


    private void OnTriggerStay2D(Collider2D other)
    {
        if (layerMask == (layerMask | (1 << other.gameObject.layer)))
        {
            force = ApplyGradient(applyGradient, other, force);
            if (whirlwind)
            {
                stickRB.AddForceAtPosition(force * dir.normalized, other.transform.position);
                draw(other);
            }
            else
            {
                if (other.transform.position.x < target.position.x)
                {
                    stickRB.AddForceAtPosition(force * dir.normalized, other.transform.position);
                    draw(other);
                }
            }

            //stickRB.AddForce(force * new Vector2(0, dir.y));
        }
    }

    private float ApplyGradient(bool isDoing, Collider2D other, float baseVal = 1f)
    {
        if (isDoing)
        {
            var distToTarget = target.position - other.transform.position;
            return baseVal / distToTarget.magnitude;
        }
        else return baseVal;
    }

    private void draw(Collider2D other)
    {
        if (drawGizmo)
        {
            Debug.DrawLine(other.transform.position, dir, Color.red);
            Debug.DrawLine(transform.position, dir, Color.green);
        }
    }
}