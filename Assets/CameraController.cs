﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [SerializeField]
    private Transform stick;

    [SerializeField]
    private GameObject musicObject;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    public bool isCustom;

    public Transform aim;
    public Transform bounds;

    public float cameraFlow = 2f;

    private Vector3 originPosition;
    private bool placed = false;
    private float _targetSize;
    private Vector3 _targetPosition;
    private float _timer;

    private void Awake()
    {
        _targetSize = mainCamera.orthographicSize;
        _targetPosition = mainCamera.transform.position;
    }

    void Update()
    {
        if (!isCustom)
        {
//            SetCameraPosition(new Vector3(stick.position.x, stick.position.y, mainCamera.transform.position.z),
//                _targetSize);
            mainCamera.transform.position =
                new Vector3(stick.position.x, stick.position.y, mainCamera.transform.position.z);
        }
        else
        {
            if (!placed)
            {
                aim.transform.position = (Vector2) mainCamera.ScreenToWorldPoint(Input.mousePosition);
            }

            if (Input.GetMouseButton(1))
            {
                placed = true;
                aim.transform.position = (Vector2) mainCamera.ScreenToWorldPoint(Input.mousePosition);
            }

            var mouseDelta = Input.mouseScrollDelta;
            if (mouseDelta != Vector2.zero)
            {
                ZoomToPointer(mouseDelta.y > 0);
            }
        }

        _timer += Time.deltaTime;

        if (Mathf.Abs(_targetSize - mainCamera.orthographicSize) > 0.001f)
        {
            mainCamera.orthographicSize =
                Mathf.Lerp(mainCamera.orthographicSize, _targetSize, _timer / cameraFlow);
        }
    }

    private void SetCameraPosition(Vector3 position, float size)
    {
        _timer = 0;
        _targetSize = size;
        _targetPosition = position;
        _targetPosition.x += 5;
    }

    public void SetCustom(bool custom)
    {
        musicObject.SetActive(!custom);
        placed = false;
        isCustom = custom;
        stick.GetComponent<Rigidbody2D>().isKinematic = custom;
        SetCameraPosition(mainCamera.transform.position, custom ? 15 : 7);
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (!isCustom) return;
        Vector3 pointerPos = eventData.pointerCurrentRaycast.worldPosition;
        if (pointerPos != Vector3.zero)
        {
            pointerPos -= mainCamera.transform.position;
            Vector2 newPosition = originPosition - pointerPos;
            ClampedMove(newPosition);
        }
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (!isCustom) return;
        originPosition = eventData.pointerCurrentRaycast.worldPosition;
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
    }

    public void ClampedMove(Vector2 newPosition)
    {
        var xValue = newPosition.x;
        var yValue = newPosition.y;
        var screenAspect = (float) Screen.width / Screen.height;
        var cameraHeightHalf = mainCamera.orthographicSize;
        var cameraWidthHalf = cameraHeightHalf * screenAspect;

        var rectXMin = bounds.GetChild(0).GetComponent<RectTransform>().rect.xMin + cameraWidthHalf;
        xValue = Mathf.Clamp(xValue, rectXMin,
            bounds.GetChild(bounds.childCount - 1).GetComponent<RectTransform>().rect.xMax - cameraWidthHalf +
            bounds.GetChild(bounds.childCount - 1).localPosition.x);
        yValue = Mathf.Clamp(yValue, bounds.GetChild(0).GetComponent<RectTransform>().rect.yMin + cameraHeightHalf,
            bounds.GetChild(bounds.childCount - 1).GetComponent<RectTransform>().rect.yMax - cameraHeightHalf);
        mainCamera.transform.position = new Vector3(xValue, yValue, mainCamera.transform.position.z);
        _targetPosition = mainCamera.transform.position;
    }

    public void ZoomToPointer(bool isZoomIn)
    {
        var cameraPosition = mainCamera.transform.position;
        var cameraSize = mainCamera.orthographicSize;

        var speed = isZoomIn ? -60f : 60f;
        var frameSpeed = Mathf.Clamp(speed * Time.deltaTime,
            7 - cameraSize,
            15 - cameraSize);

        var newSize = cameraSize + frameSpeed;
        var multiplier = -1.0f / cameraSize * frameSpeed;
        ClampedMove(cameraPosition +
                    (mainCamera.ScreenToWorldPoint(Input.mousePosition) - cameraPosition) * multiplier);

        mainCamera.orthographicSize = newSize;
        _targetSize = newSize;

        //CalculateFrustum();
    }
}