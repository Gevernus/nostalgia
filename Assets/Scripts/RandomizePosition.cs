﻿using UnityEngine;

public class RandomizePosition : MonoBehaviour
{

    public Vector2 maxOffset;

    private Vector3 offset;
    void Start()
    {
        offset = new Vector3(Random.Range(-maxOffset.x, maxOffset.x), Random.Range(-maxOffset.y, maxOffset.y), 0);
        transform.position += offset;
    }

    void Update()
    {
        
    }
}
