using UnityEngine;

namespace DefaultNamespace
{
    public class PartsGenerator : MonoBehaviour
    {
        public GameObject[] prefabs;
        public StickController stick;
        private Transform _current;

        private void Awake()
        {
            var part = Instantiate(prefabs[Random.Range(0, prefabs.Length - 1)], transform);
            for (var i = 1; i < 3; i++)
            {
                part = Instantiate(prefabs[Random.Range(0, prefabs.Length)], transform);
                var newPosition = part.transform.position;
                newPosition.x += 90 * i;
                part.transform.position = newPosition;
            }

            _current = transform.GetChild(1);
        }

        private void FixedUpdate()
        {
            if (stick.transform.position.x > _current.position.x)
            {
                Destroy(transform.GetChild(0).gameObject);
                var part = Instantiate(prefabs[Random.Range(0, prefabs.Length)], transform);
                var newPosition = _current.transform.position;
                newPosition.x += 180;
                _current = transform.GetChild(2);
                part.transform.position = newPosition;
            }
        }
    }
}