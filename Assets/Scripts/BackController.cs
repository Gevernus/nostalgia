using UnityEngine;

namespace DefaultNamespace
{
    public class BackController : MonoBehaviour
    {
        public StickController stick;
        public Rigidbody2D rb;

        private void FixedUpdate()
        {
            rb.velocity = Vector2.right * stick.speed;
        }
    }
}