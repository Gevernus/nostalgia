﻿using UnityEngine;

public class Slow : MonoBehaviour
{
    public StickController stickController;
    public float slowSpeed;
    public bool randomSpeed;
    public float slowSpeedMin;
    public float slowSpeedMax;
    
    public LayerMask layerMask;

    private void Start()
    {
        if (!stickController) {
            stickController = GameObject.Find("Stick").GetComponent<StickController>();
        }
        if(randomSpeed)
            slowSpeed = Random.Range(slowSpeedMin, slowSpeedMax);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (layerMask == (layerMask | (1 << collision.gameObject.layer))) {
            stickController.SetSlowedSpeed(slowSpeed);
            stickController.IsSlowed(true);
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (layerMask == (layerMask | (1 << collision.gameObject.layer)))
        {
            stickController.IsSlowed(false);
        }
    }
}
