using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class GameController : MonoBehaviour
    {
        public Button restartButton;
        public Button placeButton;
        public StickController stick;
        public int count = 2;
        public Transform[] startPositions;
        public TextMeshProUGUI stickCount;
        public TextMeshProUGUI distance;
        public CameraController cameraController;
        public Transform aim;
        public GameObject[] sticks;
        public GameObject roundWin;
        public GameObject roundLose;
        public float tier1;
        public float tier2;
        public float tier3;

        private void Awake()
        {
            stickCount.text = count.ToString();
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void Update()
        {
            distance.text = Mathf.CeilToInt(Vector2.Distance(stick.transform.position, startPositions[0].position)) +
                            "сm";
        }

        public void Place()
        {
            distance.gameObject.SetActive(true);
            stick.StartFlow();
            cameraController.SetCustom(false);
            count--;
            UpgradeUISticks();
            stickCount.text = count.ToString();
            var stickPosition = stick.transform.position;
            var maxDistance = -1f;
            var startPosition = Vector3.zero;
            for (var i = 0; i < startPositions.Length; i++)
            {
                var distance = Vector2.Distance(stickPosition, startPositions[i].position);
                if (maxDistance == -1 || maxDistance > distance)
                {
                    startPosition = startPositions[i].position;
                    maxDistance = distance;
                }
            }

            stick.transform.position = startPosition;
            stick.transform.right = Vector2.right;
        }

        public void CheckReward(float distance)
        {
            if (distance <= tier1)
            {
                count += 2;
                roundWin.SetActive(true);
                roundLose.SetActive(false);
                UpgradeUISticks();
            }
            else
            {
                roundWin.SetActive(false);
                roundLose.SetActive(true);
            }
        }

        private void UpgradeUISticks()
        {
            for (var i = 0; i < sticks.Length; i++)
            {
                sticks[i].SetActive(i < count);
            }
        }

        public void CheckStop()
        {
            CheckReward(Vector2.Distance(aim.transform.position, stick.transform.position));
            stickCount.text = count.ToString();
            if (count > 0)
            {
                cameraController.SetCustom(true);
                restartButton.gameObject.SetActive(false);
                placeButton.gameObject.SetActive(true);
            }
            else
            {
                restartButton.gameObject.SetActive(true);
                placeButton.gameObject.SetActive(false);
            }
        }
    }
}