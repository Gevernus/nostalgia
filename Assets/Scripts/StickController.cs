﻿using DefaultNamespace;
using UnityEngine;

public class StickController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField]
    public float speed;
    
    [SerializeField]
    private GameController controller;

    [SerializeField]
    private float stopDelay = 2f;

    private float _timer;
    private bool _isStarted;

    private bool _isSlowed;
    private float _slowedSpeed;

    void FixedUpdate()
    {
        if (_isStarted)
        {
            if (Mathf.Abs(rb.velocity.magnitude) < 0.001f)
            {
                _timer += Time.deltaTime;
                if (_timer >= stopDelay)
                {
                    _isStarted = false;
                    controller.CheckStop();
                    rb.isKinematic = true;
                    return;
                }
            }
            else
            {
                _timer = 0;
            }

            DecideSpeed();
        }
    }

    public void StartFlow()
    {
        rb.isKinematic = false;
        _isStarted = true;
        DecideSpeed();
    }

    void DecideSpeed() { 
        if(_isSlowed) rb.velocity = _slowedSpeed * Vector2.right;
        else rb.velocity = speed * Vector2.right;
    }

    public void IsSlowed(bool value) {
        _isSlowed = value;
    }

    public void SetSlowedSpeed(float value) {
        _slowedSpeed = value;
    }
}